<?php

/*
 * This file is part of jwt-auth.
 *
 * (c) Sean NoMercy <tymon148@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace NoMercy\JWTAuth\Providers;

use NoMercy\JWTAuth\Http\Parser\AuthHeaders;
use NoMercy\JWTAuth\Http\Parser\InputSource;
use NoMercy\JWTAuth\Http\Parser\QueryString;
use NoMercy\JWTAuth\Http\Parser\LumenRouteParams;

class LumenServiceProvider extends AbstractServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $this->app->configure('jwt');

        $path = realpath(__DIR__.'/../../config/config.php');
        $this->mergeConfigFrom($path, 'jwt');

        $this->app->routeMiddleware($this->middlewareAliases);

        $this->extendAuthGuard();

        $this->app['nomercy.jwt.parser']->setChain([
            new AuthHeaders,
            new QueryString,
            new InputSource,
            new LumenRouteParams,
        ]);
    }
}
