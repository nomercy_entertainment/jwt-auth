<?php

/*
 * This file is part of jwt-auth.
 *
 * (c) Sean NoMercy <tymon148@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace NoMercy\JWTAuth\Exceptions;

class PayloadException extends JWTException
{
    //
}
